/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.pojos;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 *
 * @author office
 */
@XmlRegistry
public class ObjectFactory {
	
	@XmlElementDecl(name = "ref")
	public JAXBElement createInteger(Integer v) {
		return new JAXBElement(new QName("ref"), Integer.class, v);
	}
}
