/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test;

import com.test.pojos.MyXML;
import com.test.pojos.ObjectFactory;
import java.io.IOException;
import java.io.OutputStream;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author office
 */
@Path("service")
public class MyService implements StreamingOutput {

	@Override
	public void write(OutputStream output) throws IOException, WebApplicationException {
		try {
			ObjectFactory factory = new ObjectFactory();
			
			MyXML xml = new MyXML();
			xml.ref_int = factory.createInteger(5);
			
			JAXBContext cnt = JAXBContext.newInstance("com.test.pojos");
//			output.write(cnt.toString().getBytes());
			
			Marshaller marshaller = cnt.createMarshaller();
			marshaller.marshal(xml, output);
			
			output.flush();
		}
		catch(JAXBException e) {
			throw new WebApplicationException(e.getMessage());
		}
	}
	
	
	@GET
	@Produces({
		MediaType.TEXT_XML,
		MediaType.TEXT_PLAIN
	})
	public Response doGet() {		
		Response.ResponseBuilder builder = Response.ok();
		builder.entity(this);
		return builder.build();
	}
}
